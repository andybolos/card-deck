import React from 'react';
import './App.css';

const App = ()  => {
  const initialState = {
    DIAMONDS: [],
    HEARTS: [],
    CLUBS: [],
    SPADES: [],
  };
  const baseApi = "https://deckofcardsapi.com/api/deck/";
  const deck_count = 1;
  const draw_count = 2;
  const [deckId, setDeckId] = React.useState(null);
  const [queens, updateQueens] = React.useState([]);
  const [cards, updateCards] = React.useState(initialState);
  const [error, setError] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);
  const [autoDraw, setAutoDraw] = React.useState(false)

  const clearDeck = () => {
    updateCards(initialState);
    updateQueens([]);
    setDeckId(null);
  }

  const shuffleCards = async () => {
    setIsLoading(true);
    clearDeck();
    const url = deckId ?
     `${baseApi}/${deckId}/shuffle/` 
     : `${baseApi}/new/shuffle/?deck_count=${deck_count}`
    const response = await fetch(url);
    response.json()
      .then(res => {
        setIsLoading(false)
        if (res.success) {
          setDeckId(res.deck_id);
        } 
        if (!res.success) {
          setError('Something went wrong');
        }
      });
  }

  const drawCards = React.useCallback(async () => {
    setIsLoading(true);
    const response = await fetch(`${baseApi}/${deckId}/draw/?count=${draw_count}`)
    response.json()
      .then(res => {
        setIsLoading(false);
        if (res.success) {
          const newCards = cards;
          res.cards.forEach(card => {
            newCards[card.suit].push(card.value);
            if (card.value === 'QUEEN') {
              updateQueens([...queens, card]);
            }
          });
          updateCards(state => ({...state, ...newCards}));
        } 
        if (!res.success) {
          setError('Something went wacky...');
        }
      });
  }, [cards, deckId, queens]);

  const winner = React.useCallback(() => {
    if (queens.length >= 4) {
      return true
    } 
    return false
  }, [queens]);

  React.useEffect(() => {
    if (autoDraw) {
      let timer = setInterval(() => {
        drawCards();
      }, 1000)
      return () => clearInterval(timer)
    }
  }, [drawCards, autoDraw])

  React.useEffect(() => {
    if (winner()) {
      setAutoDraw(false)
    }
  }, [winner])

  const autoMagicallyDrawCards = () => {
    setAutoDraw(true);
  }

  const customSort = (a, b) => {
    let sorted = ['ACE', '1', '2', '3','4','5','6','7','8','9','10','JACK','QUEEN','KING'];
    return sorted.indexOf(a) - sorted.indexOf(b);
  };

  if (error) return <div>Error...</div>


  return (
    <div className="App">
      <section>
        <div className="Instructions">
        {isLoading && !deckId && <h1>LOADING...</h1>}
        {winner() && <h1>Winner!!! You did it!</h1>}
          <h2>Lets draw some Queens!</h2>
          <p>Object: Draw cards 2 at a time until all Queens are drawn.</p>
          <p>Shuffle deck to start.</p>
        </div>
        {!autoDraw && <div className="Actions">
          <button onClick={() => shuffleCards()} disabled={isLoading}>Shuffle deck</button>
        </div>}
      </section>
      <section className="CardsBlock">
        <div className="Actions">
          {deckId && !winner() && !autoDraw &&  
            <>
              <button onClick={() => drawCards()} disabled={isLoading}>Draw cards</button>
              <button onClick={() => autoMagicallyDrawCards()} disabled={isLoading}>Auto draw cards</button>
            </>
          }
        </div>
        <div className="Cards">
        {deckId &&
        <>
          <div>
            <h2>Diamonds</h2>
            <ul>
              {cards.DIAMONDS.sort(customSort).map(x => <li key={`diamonds_${x}`}>{x}</li>)}
            </ul>
          </div>
          <div>
            <h2>Hearts</h2>
            <ul>
              {cards.HEARTS.sort(customSort).map(x => <li key={`hearts_${x}`}>{x}</li>)}
            </ul>
          </div>
          <div>
            <h2>Clubs</h2>
            <ul>
              {cards.CLUBS.sort(customSort).map(x => <li key={`clubs_${x}`}>{x}</li>)}
            </ul>
          </div>
          <div>
            <h2>Spades</h2>
            <ul>
              {cards.SPADES.sort(customSort).map(x => <li key={`spades_${x}`}>{x}</li>)}
            </ul>
          </div>
        </>}
        </div>
      </section>
    </div>
  );
}

export default App;
